package org.workshop.coffee.repository;



//write all possible unit test cases for this file with import statement

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.workshop.coffee.domain.Order;
import org.workshop.coffee.domain.Person;

@ExtendWith(MockitoExtension.class)
class OrderRepositoryTest {

    @Mock
    private OrderRepository orderRepository;
    
    @InjectMocks
    private OrderRepositoryTest orderRepositoryTest;
    
    @Test
    void testFindOrderByPerson() {
        Person person = new Person();
        List<Order> orders = new ArrayList<>();
        Order order = new Order();
        orders.add(order);
        
        when(orderRepository.findOrderByPerson(person)).thenReturn(orders);
        
        List<Order> result = orderRepository.findOrderByPerson(person);
        
        assertNotNull(result);
        assertEquals(1, result.size());
    }

    @Test
    void testFindOrderByOrderDateBetween() {
        Date startDate = new Date();
        Date endDate = new Date();
        
        List<Order> orders = new ArrayList<>();
        Order order = new Order();
        orders.add(order);
        
        when(orderRepository.findOrderByOrderDateBetween(startDate, endDate)).thenReturn(orders);
        
        List<Order> result = orderRepository.findOrderByOrderDateBetween(startDate, endDate);
        
        assertNotNull(result);
        assertEquals(1, result.size());
    }

}

