package org.workshop.coffee.repository;

  //write all possible unit test cases for this file along with import statement

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.workshop.coffee.domain.Product;

@ExtendWith(MockitoExtension.class)
class ProductRepositoryTest {

    @Mock
    private ProductRepository productRepository;
    
    @InjectMocks
    private Product product;
    
    @Test
    void testFindProductByProductName() {
        String name = "Coffee";
        Product mockProduct = new Product();
        mockProduct.setProductName(name);
        
        when(productRepository.findProductByProductName(name)).thenReturn(mockProduct);
        
        Product result = productRepository.findProductByProductName(name);
        
        assertNotNull(result);
        assertEquals(name, result.getProductName());
    }

}

