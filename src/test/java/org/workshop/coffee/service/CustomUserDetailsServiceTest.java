package org.workshop.coffee.service;

    //write all possible unit test cases for this file along with import statement








import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.util.Collections;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.workshop.coffee.domain.Person;
import org.workshop.coffee.repository.PersonRepository;

@ExtendWith(MockitoExtension.class)
class CustomUserDetailsServiceTest {

    @Mock
    private PersonRepository personRepository;

    @InjectMocks
    private CustomUserDetailsService customUserDetailsService;


    @Test
    void testLoadUserByUsername_notFound() {
        when(personRepository.findByUsername("invalid")).thenReturn(null);

        assertThrows(UsernameNotFoundException.class, () -> {
            customUserDetailsService.loadUserByUsername("invalid");
        });
    }

}
