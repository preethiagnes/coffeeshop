package org.workshop.coffee.service;

//write all possible unit test cases for this file along with import statements

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.workshop.coffee.domain.Person;
import org.workshop.coffee.domain.Role;
import org.workshop.coffee.exception.EmailTakenException;
import org.workshop.coffee.exception.UsernameTakenException;
import org.workshop.coffee.repository.PersonRepository;

@ExtendWith(MockitoExtension.class)
public class PersonServiceTest {

    @Mock
    private PersonRepository personRepository;

    @InjectMocks
    private PersonService personService;

    @Test
    public void testGetAllPersons() {
        List<Person> persons = new ArrayList<>();
        persons.add(new Person());
        when(personRepository.findAll()).thenReturn(persons);
        
        List<Person> result = personService.getAllPersons();
        
        assertEquals(persons, result);
    }

    @Test
    public void testSavePerson() {
        Person input = new Person();
        Person saved = new Person();
        when(personRepository.save(input)).thenReturn(saved);
        
        Person result = personService.savePerson(input);
        
        assertEquals(saved, result);
    }

    @Test 
    public void testFindByUsername() {
        Person person = new Person();
        when(personRepository.findByUsername("test")).thenReturn(person);
        
        Person result = personService.findByUsername("test");
        
        assertEquals(person, result);
    }
    
    @Test
    public void testRegisterNewPersonUsernameTaken() {
        Person input = new Person();
        input.setUsername("taken");
        when(personRepository.findByUsername("taken")).thenReturn(new Person());
        
        Exception exception = assertThrows(UsernameTakenException.class, () -> {
            personService.registerNewPerson(input); 
        });
        
        String expectedMessage = "Username is already taken: " + input.getUsername();        
        assertEquals(expectedMessage, exception.getMessage());
    }
    
    @Test
    public void testRegisterNewPersonEmailTaken() {
        Person input = new Person();
        input.setEmail("taken@test.com");
        List<Person> persons = new ArrayList<>();
        persons.add(new Person());
        when(personRepository.findByEmail("taken@test.com")).thenReturn(persons);
        
        Exception exception = assertThrows(EmailTakenException.class, () -> {
            personService.registerNewPerson(input); 
        });
        
        String expectedMessage = "Email is already exists: " + input.getEmail();        
        assertEquals(expectedMessage, exception.getMessage());
    }

}

