package org.workshop.coffee.service;


// write all possible unit test cases for this file along with this file along with import statements

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.workshop.coffee.domain.Order;
import org.workshop.coffee.domain.Person;
import org.workshop.coffee.repository.OrderRepository;

@ExtendWith(MockitoExtension.class)
class OrderServiceTest {

    @Mock
    private OrderRepository orderRepository;

    @InjectMocks
    private OrderService orderService;

    @Test
    void testSave() {
        Order order = new Order();
        when(orderRepository.save(order)).thenReturn(order);
        Order savedOrder = orderService.save(order);

        assertNotNull(savedOrder);
        assertEquals(order, savedOrder);
    }

    @Test
    void testDelete() {
        Order order = new Order();
        orderService.delete(order);
        // No need to assert anything as delete() doesn't return anything
    }

    @Test
    void testFindByPerson() {
        Person person = new Person();
        List<Order> expectedOrders = new ArrayList<>();

        when(orderRepository.findOrderByPerson(person)).thenReturn(expectedOrders);

        List<Order> actualOrders = orderService.findByPerson(person);

        assertEquals(expectedOrders, actualOrders);
    }

    @Test
    void testFindByDate() {
        Date minDate = new Date(); 
        Date maxDate = new Date();
        List<Order> expectedOrders = new ArrayList<>();

        when(orderRepository.findOrderByOrderDateBetween(minDate, maxDate)).thenReturn(expectedOrders);

        List<Order> actualOrders = orderService.findByDate(minDate, maxDate);

        assertEquals(expectedOrders, actualOrders);
    }

    @Test
    void testFindAll() {
        List<Order> expectedOrders = new ArrayList<>();

        when(orderRepository.findAll()).thenReturn(expectedOrders);

        List<Order> actualOrders = orderService.findAll();

        assertEquals(expectedOrders, actualOrders);
    }

}




