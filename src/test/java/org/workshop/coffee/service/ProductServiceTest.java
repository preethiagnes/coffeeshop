package org.workshop.coffee.service;


//write all possible unit test cases for this file along with import statements and package

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.workshop.coffee.domain.Product;
import org.workshop.coffee.repository.ProductRepository;

@ExtendWith(MockitoExtension.class)
class ProductServiceTest {

    @Mock
    private ProductRepository productRepository;
    
    @InjectMocks
    private ProductService productService;
    
    @Test
    void testSave() {
        Product product = new Product();
        when(productRepository.save(product)).thenReturn(product);
        
        Product savedProduct = productService.save(product);
        
        assertNotNull(savedProduct);
        assertEquals(product, savedProduct);
    }
    
    @Test
    void testGetProduct() {
        Long productId = 1L;
        Product product = new Product();
        when(productRepository.findById(productId)).thenReturn(Optional.of(product));
        
        Product savedProduct = productService.getProduct(productId);
        
        assertNotNull(savedProduct);
        assertEquals(product, savedProduct);
    }
    
    @Test
    void testGetAllProducts() {
        List<Product> products = new ArrayList<>();
        products.add(new Product());
        when(productRepository.findAll()).thenReturn(products);
        
        List<Product> savedProducts = productService.getAllProducts();
        
        assertNotNull(savedProducts);
        assertEquals(products, savedProducts);
    }
    
}


